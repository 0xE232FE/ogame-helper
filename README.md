### ogame-helper

Provide a simple offline helper for OGame players.

#### License

GNU General Public License v3.0

See [LICENSE.md](LICENSE.md) to see the full text.
